<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>
-->

# Bugzillabot

Bugzillabot automatically manages NEEDSINFO bugs.

# Requires

- Ruby 2.0+

# Configuration

Configuration is supplied by `.config.yaml` inside the git working directory
or by `~/.config/bugzillabot.yaml`. The config contains a hash of "type" keys
which contain a hash of the actual config values of a given run type.
See `test/config.yaml` for an example.

# Run Types

By default bugzillabot runs in TESTING mode which means it uses the `testing`
type config. This gets used to validate behavior against a staging
bugzilla instance. If all is good bugzillabot can be run with the environment
variable `PRODUCTION=1` to switch it into production mode.

# Installation

- `bundle install`
- create a .config.yaml or user-level config
- add testing settings to config
- add production settings to config
- validate against testing
- `PRODUCTION=1 bin/bugzillabot`

# For production...

The program runs on a KDE server. Get in touch with sysadmins about it.
It's run through a cron job. Server-side setup gets done via the
setup script in this repo. The crontab file in the repo contains the crontab
setup used on the server.

# Debugging

You can enable HTTP level debugging by setting `DEBUG=1` in the environment.
This does strip tokens, so no access data gets leaked by this.
