# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'bugzillabot'
require 'template'

Bugzillabot::Config.load_paths = [File.join(__dir__, 'config.yaml')]

require 'minitest/pride'
require 'minitest/autorun'

require 'webmock/minitest'
