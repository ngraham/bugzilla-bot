# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018-2022 Harald Sitter <sitter@kde.org>

require 'pp'
require 'date'
require 'logger'
require 'time'

require_relative '../bugzillabot'
require_relative '../constants'
require_relative '../eoltemplate'
require_relative '../monkey'

module Runner
  class Priority
    def self.run(logger:)
      last_change = (DateTime.now.new_offset(0) - 3/24.0).to_datetime.iso8601
      to_change = { 'VHI' => [], 'HI' => [] }

      Bugzillabot::Bug.search(last_change_time: last_change) do |bug|
        next if bug.product == 'krita'

        if bug.priority == 'VHI' # if it is already VHI we need not do anything to it!
          logger.info "Bug #{bug.id} already VHI -> ignoring"
          next
        end

        if bug.comments.size < 5
          logger.info "Bug #{bug.id} has too few comments -> ignoring"
          next
        end

        dupes = bug.comments.select do |comment|
          # unfortuntely the api doesn't carry the dupe numbers, so we have to look at the comments :((
          matchdata = comment.text.match(/Bug (?<dupe>\d+) has been marked as a duplicate of this bug. \*\*\*/)
          next false unless matchdata

          dupe = Bugzillabot::Bug.get(matchdata[:dupe])
          dupe.dupe_of == bug.id
        end.size
        if dupes < 5
          logger.info "Bug #{bug.id} has too few dupes #{dupes} -> ignoring"
          next
        end

        target = if dupes >= 10
                   'VHI'
                 elsif dupes >= 5
                   'HI'
                 end

        if bug.priority == target
          logger.info "Bug #{bug.id} already #{target} -> ignoring"
          next
        end

        logger.info "Bug #{bug.id} has multiple dupes #{dupes} -> marking #{target}"
        to_change[target] << bug
      end

      to_change.each do |priority, bugs|
        bugs.each do |bug|
          bug.update({ ids: bug.id, priority: priority })
        end
      end
    end
  end
end
