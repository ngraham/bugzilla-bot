# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018-2022 Harald Sitter <sitter@kde.org>

require 'pp'
require 'date'
require 'logger'
require 'time'

require_relative '../almost_eoltemplate'
require_relative '../bugzillabot'
require_relative '../constants'
require_relative '../eoltemplate'
require_relative '../monkey'

module Runner
  class Versions
    def self.run(logger:)
      products = YAML.load_file("#{__dir__}/../../data/plasma-products.yml")
      versions = YAML.load_file("#{__dir__}/../../data/versions.yml")
      plasma_versions = versions['plasma']

      almost_eol = plasma_versions['almost_eol']
      oldest_supported = plasma_versions['oldest_supported']
      protected = plasma_versions['protected']
      lts_constraints = plasma_versions['lts_constraints']
      lts_constraints_to_s = lts_constraints.collect { |x| x.sub('~> ', '')[0..-3] } # strip expression and trailing .0
      last_change = (DateTime.now.new_offset(0) - 3 / 24.0).to_datetime.iso8601

      to_nudge = []
      to_close = []

      products.each do |product|
        Bugzillabot::Bug.search(status: 'UNCONFIRMED', product: product, last_change_time: last_change) do |bug|
          if protected.include?(bug.version)
            logger.info "Bug #{bug.id} has a protected version #{bug.version} -> ignoring"
            next
          end

          version = Gem::Version.new(bug.version)
          if lts_constraints.any? { |constraint| Gem::Dependency.new('', constraint).match?('', version) }
            logger.info "Bug #{bug.id} matches an LTS constraint #{bug.version} -> ignoring"
            next
          end
          if version >= Gem::Version.new(oldest_supported)
            logger.info "Bug #{bug.id} matches oldest version constraint #{bug.version} -> ignoring"
            next
          end

          if bug.comments.size > 1 # comments are expensive, check them last
            logger.info "Bug #{bug.id} has multiple comments -> ignoring"
            next
          end

          # This version is not matching a constraint but it may be between almost_eol and oldest_supported, in which
          # case we nudge the user to upgrade instead of outright closing the bug report.
          if almost_eol && version >= Gem::Version.new(almost_eol) # NB: almost_eol may be nil!
            logger.warn "Bug #{bug.id} is going to be nudged because of its version #{bug.version}"
            to_nudge << bug
            next
          end

          logger.warn "Bug #{bug.id} is going to be closed because of its version #{bug.version}"
          to_close << bug
        rescue ArgumentError # bad version input
          raise "Bug #{bug.id} had an unexpected version #{bug.version}"
        end
      end

      to_nudge.each do |bug|
        almost_eol = AlmostEOLTemplate.new(report: bug.version, plasma: oldest_supported, plasma_lts: lts_constraints_to_s)
        bug.update({ comment: { body: almost_eol } })
      end

      to_close.each do |bug|
        eol = EOLTemplate.new(report: bug.version, plasma: oldest_supported, plasma_lts: lts_constraints_to_s)
        bug.update({ ids: bug.id, status: 'RESOLVED', resolution: 'DOWNSTREAM', comment: { body: eol } })
      end
    end
  end
end
