# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

require 'sentry-ruby'
require 'yaml'

sentry_config = File.absolute_path(File.join(__dir__, '../.sentry.yaml'))
if File.exist?(sentry_config)
  puts "Loading #{sentry_config}"
  data = YAML.load_file(sentry_config)
  Sentry.init do |config|
    config.breadcrumbs_logger = %i[http_logger]
    config.dsn = data.fetch('dsn')
    config.traces_sample_rate = 1.0
    config.debug = true
    config.environment = ENV['PRODUCTION'] ? 'production' : 'testing'
    config.enabled_environments = %w[production]
    config.excluded_exceptions += %w[Interrupt]
  end

  at_exit do
    Sentry.capture_exception($!, level: :fatal) unless $!.is_a?(SystemExit) || $!.nil?
    Sentry.session_flusher.flush
  end
else
  puts 'Sentry not configured.'
end
