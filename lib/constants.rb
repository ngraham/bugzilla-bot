# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require_relative 'bugzillabot'

# FIXME: load username from config
BOT_USER = 'bug-janitor@kde.org'
BUGZILLA_URL = Bugzillabot.config.bugzilla_url
