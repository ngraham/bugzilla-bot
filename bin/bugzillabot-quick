#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018-2022 Harald Sitter <sitter@kde.org>

require_relative '../lib/sentry'

require_relative '../lib/bugzillabot'
require_relative '../lib/constants'
require_relative '../lib/eoltemplate'
require_relative '../lib/monkey'
require_relative '../lib/modules'

logger_output = if Bugzillabot.config.log?
                  "#{Bugzillabot.config.log_directory}/quick.log"
                elsif ENV['AUTOMATIC']
                  File::NULL
                else
                  $stdout
                end
logger = Logger.new(logger_output, 10, 512 * 1024)

# The quick bot is based on modules, each module implements a run method. We invoke them through fancy meta programming.
Sentry.with_session_tracking do
  constants = Runner.constants.collect { |x| Runner.const_get(x) }
  constants.each do |klass|
    next unless klass.is_a?(Class)

    logger.progname = klass.to_s
    transaction = Sentry.start_transaction(name: klass.to_s, op: klass.to_s)
    klass.run(logger: logger)
    transaction&.finish
  end
end
